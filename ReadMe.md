# Enable 3D

Simple projects build in Enable 3D

- [Play](https://enable3d-fixolas.netlify.app/);

## Enable 3D
- [Enable 3D](https://enable3d.io/);
  - [Exemplos Oficial](https://enable3d.io/examples.html);

## Phaser
- [Phaser](https://phaser.io);
  - [CDN Phaser](https://cdnjs.com/libraries/phaser);

``` html

<!-- Normal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/phaser/3.53.1/phaser.js" integrity="sha512-sIbZTy6oqGQ+YoMiI/ETzWsYGlzqWeZK09fU/5PIQlWHRwXzeRbSyyi/D2ZvqWhfXJ428p9aI5wFEClKc4Je6g==" crossorigin="anonymous"></script>

<!-- Min -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/phaser/3.53.1/phaser.min.js" integrity="sha512-891C/4mi1rZQU6Rw//yZ1IDyCoe/gLBoWrH8cbieADX2wsjKrdvzcr06H4GeEgz3goUz6em/0TR9X7GPI8HPXg==" crossorigin="anonymous"></script>


```

## Node

## Threejs
