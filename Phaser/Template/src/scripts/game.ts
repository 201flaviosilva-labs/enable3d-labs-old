import * as Phaser from 'phaser';
import { enable3d, Canvas } from '@enable3d/phaser-extension';

import Main from './scenes/Main';
import Preload from './scenes/Preload';

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.WEBGL,
  scale: {
    width: 1280,
    height: 720,
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  transparent: true,
  scene: [
    Preload, 
    Main,
  ],
  ...Canvas(),
}

window.addEventListener('load', () => {
  enable3d(() => new Phaser.Game(config)).withPhysics('/assets/ammo')
});
