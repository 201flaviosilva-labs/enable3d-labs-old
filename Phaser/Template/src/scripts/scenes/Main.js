import { Scene3D } from '@enable3d/phaser-extension';

export default class Main extends Scene3D {
  constructor() {
    super({ key: "Main" });
  }

  init() {
    this.accessThirdDimension();
  }

  create() {
    // creates a nice scene
    this.third.warpSpeed();

    this.third.add.box({ x: 1, y: 2 });

    this.third.physics.add.box({ x: -1, y: 2 });

    this.third.haveSomeFun();
  }

  update() {
    console.log("beep");
  }
}
