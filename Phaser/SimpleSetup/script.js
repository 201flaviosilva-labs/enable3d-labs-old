const { enable3d, Scene3D, Cameras, Third, Canvas, Project, PhysicsLoader, THREE, FirstPersonControls } = ENABLE3D;
// import { randomNumber, randomColor } from "../../utils";

const randomColor = () => "#" + (Math.random() * 0xFFFFFF << 0).toString(16);
const randomNumber = (min = 0, max = 100) => Math.floor(Math.random() * (max - min + 1) + min);

let actualRotation = 0;

class MainScene extends Scene3D {
	constructor() {
		super({ key: 'MainScene' });
	}

	async init() {
		this.accessThirdDimension(); // Abilita as 3 dimenções?
	}

	async create() {
		this.third.warpSpeed(); // Desenha uma simples plataforma
		this.basicBoxRotate = this.third.physics.add.box({ y: 1 }); // Adiciona uma Caixa

		this.third.camera.position.set(10, 15, 20); // Muda a posição da camara

		this.third.camera.lookAt(this.third.scene.position);
		this.createBox();

		const keySpace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
		keySpace.on("down", this.createBox, this);

		this.add.text(this.game.config.width / 2, 100, "Clica Espaço para criar objetos!", { fill: "#000" }).setOrigin(0.5);

		this.haveSomeFun(); // Função nativa do Enable3D, cria varios objetos aleatórios

		// Add Keys
		this.keys = {
			w: this.input.keyboard.addKey("w"),
			a: this.input.keyboard.addKey("a"),
			s: this.input.keyboard.addKey("s"),
			d: this.input.keyboard.addKey("d"),
		}
	}

	createBox() {
		this.third.physics.add.box(
			{
				x: randomNumber(-10, 10),
				y: randomNumber(1, 50),
				z: randomNumber(-10, 10),
				width: randomNumber(1, 5),
				height: randomNumber(1, 5),
				depth: randomNumber(1, 5),
				mass: randomNumber(1, 5),
				collisionFlags: 0, // Dynamic -> 0, Static -> 1, Kinematic -> 2, Ghost -> 4
			},
			{
				lambert: { // Body Type,
					color: randomColor(),
					transparent: true,
					opacity: randomNumber(50, 100) / 100,
				}
			});

		this.haveSomeFun();
	}

	update() {
		if (this.keys.w.isDown) console.log("W");
		if (this.keys.a.isDown) console.log("A");
		if (this.keys.s.isDown) console.log("S");
		if (this.keys.d.isDown) console.log("D");

		this.rotateBox();
	}

	rotateBox() {
		actualRotation += 0.01;
		// set body to be kinematic
		this.basicBoxRotate.body.setCollisionFlags(2); // Kinematic

		// set the new position
		this.basicBoxRotate.rotation.set(actualRotation, actualRotation, actualRotation);
		this.basicBoxRotate.body.needUpdate = true;

		// this will run only on the next update if body.needUpdate = true
		this.basicBoxRotate.body.once.update(() => {
			// set body back to dynamic
			this.basicBoxRotate.body.setCollisionFlags(0); // Dynamic

			// if you do not reset the velocity and angularVelocity, the object will keep it
			this.basicBoxRotate.body.setVelocity(0, 0, 0);
			this.basicBoxRotate.body.setAngularVelocity(0, 0, 0);
		});
	}
}

const config = {
	type: Phaser.WEBGL,
	transparent: true,
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		width: window.innerWidth * Math.max(1, window.devicePixelRatio / 2),
		height: window.innerHeight * Math.max(1, window.devicePixelRatio / 2)
	},
	scene: [MainScene],
	...Canvas()
}

window.addEventListener('load', () => {
	enable3d(() => new Phaser.Game(config)).withPhysics('/lib/ammo/kripken')
});
